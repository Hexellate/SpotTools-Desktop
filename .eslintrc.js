module.exports = {
  env: {
    browser: true,
    node: true,
    es6: true
  },
  extends: ["airbnb", "react-app", "prettier"],
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    }
  },
  plugins: ["prettier", "react"],
  rules: {
    "prettier/prettier": ["error"]
  }
};
